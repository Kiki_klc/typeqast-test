import { Component } from '@angular/core';
import { LocalStorageService } from './shared/services/local-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'typeqast-test';

  constructor(private localStorageService: LocalStorageService) {
    const userList = this.localStorageService.getFromLocalStorage('userList');

    if (!userList) {
      this.localStorageService.saveToLocalStorage('userList', []);
    }
  }
}
