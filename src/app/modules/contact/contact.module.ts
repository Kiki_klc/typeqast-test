import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { ContactRoutingModule } from './contact-routing.module';

import { ContactListComponent } from './contact-list/contact-list.component';
import { ContactDetailComponent } from './contact-detail/contact-detail.component';
import { ContactEditComponent } from './contact-edit/contact-edit.component';
import { ContactItemComponent } from './contact-item/contact-item.component';
import { ContactFavoritesComponent } from './contact-favorites/contact-favorites.component';
import { ContactNavigationComponent } from './contact-navigation/contact-navigation.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    ContactListComponent,
    ContactDetailComponent,
    ContactEditComponent,
    ContactItemComponent,
    ContactFavoritesComponent,
    ContactNavigationComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ContactRoutingModule,
    SharedModule
  ],
  exports: [
    ContactNavigationComponent
  ]
})
export class ContactModule { }
