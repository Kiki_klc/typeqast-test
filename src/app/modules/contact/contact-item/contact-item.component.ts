import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from 'src/app/shared/models/user.model';
import { ModalService } from 'src/app/shared/services/modal.service';

@Component({
  selector: 'app-contact-item',
  templateUrl: './contact-item.component.html',
  styleUrls: ['./contact-item.component.scss']
})
export class ContactItemComponent implements OnInit {
  @Input() user: User;
  @Output() favoriteUser: EventEmitter<any> = new EventEmitter();
  @Output() deleteUser: EventEmitter<any> = new EventEmitter();

  constructor(private modalService: ModalService) { }

  ngOnInit(): void {
  }

  /**
   * Favorites user from event
   * @param event Favorited user from event
   */
  toggleFavoriteUser(event: Event) {
    event.stopPropagation();
    this.favoriteUser.emit(this.user);
  }

  userDelete(event, user) {
    event.stopPropagation();
    this.deleteUser.emit(user);
  }
}
