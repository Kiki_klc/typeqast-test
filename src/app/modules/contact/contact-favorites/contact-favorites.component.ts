import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { User } from 'src/app/shared/models/user.model';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { ModalService } from 'src/app/shared/services/modal.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact-favorites',
  templateUrl: './contact-favorites.component.html',
  styleUrls: ['./contact-favorites.component.scss']
})
export class ContactFavoritesComponent implements OnInit {
  filterInput = new FormControl('');
  userList: User[];
  favoriteList: User[];
  userActivated: number;
  userMarkedForDelete: User;
  url: string;

  constructor(private localStorageService: LocalStorageService, private modalService: ModalService, private router: Router) {
  }

  ngOnInit(): void {
    this.userList = this.localStorageService.getFromLocalStorage(
      'userList'
    );

    this.favoriteList = this.userList.filter(obj => {
      return obj.favorite === true;
    });

    this.onChanges();
  }

  /**
   * Filter input changes subscription
   */
  onChanges(): void {
    this.filterInput.valueChanges.subscribe(val => {
    });
  }

  /**
   * Activates/selects user
   * @param id Activated user id.
   */
  showUser(id) {
    this.userActivated = id;

    this.router.navigate(['/', 'contact', this.userActivated]);
  }

  /**
   * Favorites user
   * @param event Favorited user from event
   */
  favoriteUser(event) {
    const favoritedUser = this.userList.find(x => x.id === event.id);

    favoritedUser.favorite = !favoritedUser.favorite;
    this.localStorageService.updateInLocalStorage('userList', this.userList);
    this.favoriteList = this.localStorageService.getFromLocalStorage('userList');
  }

  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }

  onUserDelete($event) {
    this.userMarkedForDelete = $event;
    this.openModal('delete-modal');
  }

  /**
   * Deletes user
   * @param event Deletes user from event
   */
  deleteUser(user) {
    const deletedUserIndex = this.userList.findIndex(x => x.id === +user.id);

    this.userList.splice(deletedUserIndex, 1);

    this.localStorageService.updateInLocalStorage('userList', this.userList);
    this.favoriteList = this.localStorageService.getFromLocalStorage('userList');
  }
}
