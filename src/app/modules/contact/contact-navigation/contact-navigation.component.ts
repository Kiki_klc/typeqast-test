import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact-navigation',
  templateUrl: './contact-navigation.component.html',
  styleUrls: ['./contact-navigation.component.scss']
})
export class ContactNavigationComponent implements OnInit {
  url: any;

  constructor(router: Router) {
    this.url = router.url;
  }

  ngOnInit(): void {
  }
}
