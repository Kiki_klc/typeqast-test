import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { User } from 'src/app/shared/models/user.model';
import { ModalService } from 'src/app/shared/services/modal.service';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit {
  filterInput = new FormControl('');
  userList: User[];
  userActivated: number;
  userMarkedForDelete: User;
  url: string;

  constructor(private localStorageService: LocalStorageService, private router: Router, private modalService: ModalService) {
  }

  ngOnInit(): void {
    this.userList = this.localStorageService.getFromLocalStorage(
      'userList'
    );

    this.onChanges();
  }

  /**
   * Filter input changes subscription
   */
  onChanges(): void {
    this.filterInput.valueChanges.subscribe(val => {
    });
  }

  /**
   * Activates/selects user
   * @param id Activated user id.
   */
  showUser(id) {
    this.userActivated = id;

    this.router.navigate(['/', 'contact', this.userActivated]);
  }

  /**
   * Favorites user
   * @param event Favorited user from event
   */
  favoriteUser(event) {
    const favoritedUser = this.userList.find(x => x.id === event.id);

    favoritedUser.favorite = !favoritedUser.favorite;
    this.localStorageService.updateInLocalStorage('userList', this.userList);
    this.userList = this.localStorageService.getFromLocalStorage('userList');
  }

  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }

  onUserDelete($event) {
    this.userMarkedForDelete = $event;
    this.openModal('delete-modal');
  }

  /**
   * Deletes user
   * @param event Deletes user from event
   */
  deleteUser(user) {
    const deletedUserIndex = this.userList.findIndex(x => x.id === +user.id);

    this.userList.splice(deletedUserIndex, 1);

    this.localStorageService.updateInLocalStorage('userList', this.userList);
    this.userList = this.localStorageService.getFromLocalStorage('userList');
  }
}
