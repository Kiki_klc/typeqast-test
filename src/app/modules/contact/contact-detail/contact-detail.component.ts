import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/shared/models/user.model';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';


@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.scss']
})
export class ContactDetailComponent implements OnInit {
  userId: number;
  user: User;
  userList: User[];

  constructor(private route: ActivatedRoute, private localStorageService: LocalStorageService) {

    // Getting user list
    this.userList = this.localStorageService.getFromLocalStorage(
      'userList'
    );

    // Getting id from route parameters
    this.userId = route.snapshot.params['id'];

    // Find user in the list
    this.user = this.userList.find(x => x.id === +this.userId);
  }

  ngOnInit(): void {

  }

  /**
   * Favorites user
   * @param event Favorited user from event
   */
  favoriteUser(event: any) {
    const favoritedUser = this.userList.find(x => x.id === event.id);

    favoritedUser.favorite = !favoritedUser.favorite;
    this.localStorageService.updateInLocalStorage('userList', this.userList);
  }
}
