import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { User } from 'src/app/shared/models/user.model';
import { ModalService } from 'src/app/shared/services/modal.service';

@Component({
  selector: 'app-contact-edit',
  templateUrl: './contact-edit.component.html',
  styleUrls: ['./contact-edit.component.scss']
})
export class ContactEditComponent implements OnInit {
  userId: number;
  user: User;
  action = 'add';
  userForm: FormGroup;
  phoneNumbers: FormArray;
  userList: User[];

  imageError: string;
  isImageSaved: boolean;
  cardImageBase64: string;

  constructor(
    private route: ActivatedRoute,
    private localStorageService: LocalStorageService,
    private formBuilder: FormBuilder,
    private router: Router,
    private modalService: ModalService) {
    this.action = route.snapshot.params['action'];

    // Getting user list
    this.userList = this.localStorageService.getFromLocalStorage(
      'userList'
    );

    switch (this.action) {
      case 'add':
        this.userForm = this.formBuilder.group({
          name: new FormControl('', [
            Validators.required,
            Validators.minLength(3)
          ]),
          email: new FormControl('', [
            Validators.required,
            Validators.email
          ]),
          phoneNumbers: this.formBuilder.array([this.createItem({
            title: '',
            number: '',
          })])
        });
        break;

      case 'edit':
        // Getting id from route parameters
        this.userId = route.snapshot.params['id'];

        // Find user in the list
        this.user = this.userList.find(x => x.id === +this.userId);
        this.userForm = this.formBuilder.group({
          name: new FormControl(this.user.name, [
            Validators.required,
            Validators.minLength(3)
          ]),
          email: new FormControl(this.user.contactInfo.email, [
            Validators.required,
            Validators.email
          ]),
          phoneNumbers: this.formBuilder.array([])
        });
        this.initFormArray(this.user.contactInfo.number);

        this.cardImageBase64 = this.user.avatar;
        break;
    }
  }

  ngOnInit(): void {

  }

  createItem(item): FormGroup {
    const formGroup: FormGroup = new FormGroup({
      title: new FormControl(item.title, [
        Validators.required,
        Validators.minLength(3)
      ]),
      number: new FormControl(item.number, [
        Validators.required,
        Validators.minLength(3)
      ]),
    });

    return formGroup;
  }

  initFormArray(items) {
    const formArray = this.userForm.get('phoneNumbers') as FormArray;

    items.map(item => {
      formArray.push(this.createItem(item));
    });

    this.userForm.setControl('phoneNumbers', formArray);
  }

  addItemToFormArray(item) {
    const formArray = this.userForm.get('phoneNumbers') as FormArray;

    formArray.push(this.createItem(item));

  }

  removeItemFromFormArray(index) {
    const formArray = this.userForm.get('phoneNumbers') as FormArray;

    formArray.removeAt(index);
  }

  saveUser() {
    if (this.action === 'add') {
      const newId = this.userList.slice(-1)[0] ? this.userList.slice(-1)[0].id + 1 : 1;

      const userInfo = {
        id: newId,
        name: this.userForm.value.name,
        contactInfo: {
          email: this.userForm.value.email,
          number: this.userForm.value.phoneNumbers
        },
        favorite: false,
        avatar: this.cardImageBase64,
      };

      this.userList.push(userInfo);
      this.localStorageService.saveToLocalStorage('userList', this.userList);
      this.router.navigate(['/', 'contact', newId]);
    } else {
      const index = this.userList.findIndex(x => x.id === +this.userId);

      this.userList[index].name = this.userForm.value.name,
        this.userList[index].contactInfo = {
          email: this.userForm.value.email,
          number: this.userForm.value.phoneNumbers
        };
      this.userList[index].avatar = this.cardImageBase64;
      this.localStorageService.saveToLocalStorage('userList', this.userList);
      this.router.navigate(['/', 'contact', +this.userId]);
    }
  }

  fileChangeEvent(fileInput: any) {
    this.imageError = null;
    if (fileInput.target.files && fileInput.target.files[0]) {
      // Size Filter Bytes
      const maxHeight = 15200;
      const maxWidth = 25600;

      const reader = new FileReader();
      reader.onload = (e: any) => {
        const image = new Image();
        image.src = e.target.result;
        image.onload = rs => {
          const imgHeight = rs.currentTarget['height'];
          const imgWidth = rs.currentTarget['width'];

          if (imgHeight > maxHeight && imgWidth > maxWidth) {
            this.imageError =
              'Maximum dimentions allowed ' +
              maxHeight +
              '*' +
              maxWidth +
              'px';
            return false;
          } else {
            const imgBase64Path = e.target.result;

            if (this.action === 'edit') {
              this.user.avatar = imgBase64Path;
            }

            this.cardImageBase64 = imgBase64Path;
            this.isImageSaved = true;
          }
        };
      };

      reader.readAsDataURL(fileInput.target.files[0]);
    }
  }

  removeImage() {
    if (this.user) {
      this.user.avatar = null;
    }

    this.cardImageBase64 = '';
    this.isImageSaved = false;
  }

  openModal(id: string, event: Event) {
    event.stopPropagation();
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }

  userDelete(user) {
    const deletedUserIndex = this.userList.findIndex(x => x.id === user.id);

    this.userList.splice(deletedUserIndex, 1);
    this.localStorageService.updateInLocalStorage('userList', this.userList);
    this.router.navigate(['/']);
  }
}
