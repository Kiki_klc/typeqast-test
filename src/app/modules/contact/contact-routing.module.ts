import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ContactListComponent } from './contact-list/contact-list.component';
import { ContactFavoritesComponent } from './contact-favorites/contact-favorites.component';
import { ContactEditComponent } from './contact-edit/contact-edit.component';
import { ContactDetailComponent } from './contact-detail/contact-detail.component';

const routes: Routes = [
  {
    path: 'contacts',
    component: ContactListComponent,
  },
  {
    path: 'favorites',
    component: ContactFavoritesComponent,
  },
  {
    path: 'contacts/:action/:id',
    component: ContactEditComponent,
  },
  {
    path: 'contact/:id',
    component: ContactDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContactRoutingModule { }
