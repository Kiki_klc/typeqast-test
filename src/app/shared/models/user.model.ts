import { ContactInfo } from './contact-info.model';

export class User {
  id: number;
  name: string;
  avatar: string;
  contactInfo: ContactInfo;
  favorite: boolean;
}
