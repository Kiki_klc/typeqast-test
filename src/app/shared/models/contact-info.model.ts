import { PhoneNumber } from './phone-number.model';

export class ContactInfo {
  email: string;
  number: PhoneNumber[];
}
