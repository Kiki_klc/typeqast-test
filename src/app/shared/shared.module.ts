import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { LocalStorageService } from './services/local-storage.service';
import { ModalService } from './services/modal.service';
import { FilterPipe } from './pipes/filter.pipe';
import { ModalComponent } from './components/modal/modal.component';

@NgModule({
  declarations: [HeaderComponent, ModalComponent, FilterPipe],
  imports: [
    CommonModule
  ],
  exports: [
    HeaderComponent,
    FilterPipe,
    ModalComponent
  ],
  providers: [
    LocalStorageService,
    ModalService
  ]
})
export class SharedModule { }
