import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  userListUrl = 'assets/data/user-list.json';

  constructor(private http: HttpClient) {}

  getContacts() {
    return this.http.get(this.userListUrl);
  }

  saveToLocalStorage(key, data) {
    localStorage.setItem(key, JSON.stringify(data));
  }

  getFromLocalStorage(key) {
    return JSON.parse(localStorage.getItem(key));
  }

  updateInLocalStorage(key, data) {
    localStorage.setItem(key, JSON.stringify(data));
  }

  setDefault() {
    localStorage.setItem('userList', JSON.stringify([]));
  }
}
